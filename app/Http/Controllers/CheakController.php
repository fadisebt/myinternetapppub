<?php

namespace App\Http\Controllers;
use App\Models\Cheak as ModelsCheak;
use App\Models\File;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheakController extends Controller
{

  public function checkIn(Request $request) {
    try {
      $id= Auth::id();
      $request->validate([
        'fileId'=>'required'
      ]);
      ModelsCheak::create([
        'fileId'=>$request['fileId'],
        'userId'=>$id
      ]);
      File::where('id',$request->fileId)->update([
        'availability'=>false
      ]);
      return response()->json([
        'state'=>true
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state'=>false,
        'data'=>$th->getMessage()
      ],500);
    }
  }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public static function check_file($userId,$fileId)
  {
    try {
        $ch=ModelsCheak::create([
          'userId'=>$userId,
          'fileId'=>$fileId,
          'type'=>'read'
        ]);
        return true;

      } catch (Exception $th) {
        echo( $th->getMessage());
        return false;
  }
}
public function checkOut(Request $request)
  {
    try {
        $ch=File::where('id',$request['fileId'])->update([
            'availability'=>true
        ]);
        return response()->json([
          'state'=>true
        ]);

      } catch (Exception $th) {
        return response()->json([
          'state'=>$th->getMessage()
        ],500);
  }
}

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function check_out($fileId)
  {
    try {
        $ch=ModelsCheak::where('fileId',$fileId)->update([
            'availability'=>true
        ]);
        return true;

      } catch (Exception $th) {
        return false;
  }
}

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {

  }
}

?>
