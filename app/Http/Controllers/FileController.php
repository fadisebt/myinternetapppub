<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CheakController;
use App\Models\Cheak;
use App\Models\File as ModelsFile;
use Illuminate\Http\Response;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function uplodFile(Request $request)
  {
    try {
        //code...
        $file=Validator::make($request->all(),[
            'name'=>'required | string',
            'path'=>'required | file'
         ]);

         $path = $request->file('path');
         $filename = time().$path->getClientOriginalName();
         Storage::disk('public')->put($filename, File::get($path));
         $file1=ModelsFile::create([
            'name'=>$request->name,
            'path'=>$filename,
            'addBy'=>Auth::id(),
            'groubId'=>$request->groupId,
            'availability'=>true
         ]);
         return response()->json([
            'status'=>true,
            'massege'=>'sucssfully'

         ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ], 500);
    }
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function showFile($userId,$id)
  {
    try {
        //code...
        $c=CheakController::check_file($userId,$id);
        if ($c==false)
        {
            return response()->json([
                'state'=>false
            ],500);
        }
        $n=ModelsFile::where('id',$id)->value('numberOfBookingTimes');
        $file=ModelsFile::where('id',$id)->update([
            'numberOfBookingTimes'=>++$n,
            'availability'=>false
        ]);
        $request=ModelsFile::where('id',$id)->first();
        $file = Storage::disk('public')->get($request->path);
        if ($file != null){
          return (new Response($file, 200));
          }
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ], 500);
    }
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function updat(Request $request)
  {
    try {
      $id=Auth::id();
      Cheak::where('userId',$id)->where('fileId',$request->id)->update([
        'type'=>'update'
      ]);
      if ($request->name != null) {
         $N=ModelsFile::where('id',$request->id)->value('numberOfTimesModified');
        $N++;
        if ($request->path != null) {
          $path = $request->file('path');
          $filename = time() . $path->getClientOriginalName();
          Storage::disk('public')->put($filename, File::get($path));
          $file = ModelsFile::where('id', $request->id)->update([
            'path' => $filename,
            'name' => $request->name,
            'numberOfTimesModified'=>$N
          ]);
        } else {
          $file = ModelsFile::where('id', $request->id)->update([
            'name' => $request->name,
            'numberOfTimesModified'=>$N
          ]);
        }
        return response()->json([
          'state' => true
        ]);
      }
      $N = ModelsFile::where('id', $request->id)->value('numberOfTimesModified');
      $N++;
      $path = $request->file('path');
      $filename = time() . $path->getClientOriginalName();
      Storage::disk('public')->put($filename, File::get($path));

      $file = ModelsFile::where('id', $request->id)->update([
        'path' => $filename,
        'numberOfTimesModified' => $N
      ]);
      return response()->json([
        'state' => true
      ]);
    } catch (Exception $e) {
      return response()->json([
        'state'=>false,
        'data'=>$e->getMessage()
      ],500);
    }
  }
  public function report($id)
  {
    $type="update";
        $report=Cheak::where('fileId',$id)->where('type',$type)->get();
        return Response()->json([
            'state'=>true,
            'report'=>$report
        ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  }
}
