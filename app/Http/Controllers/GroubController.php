<?php

namespace App\Http\Controllers;

use App\Models\Cheak;
use App\Models\File;
use App\Models\Groub;
use App\Models\GroubUser;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroubController extends Controller
{
  public function search(Request $request)
  {
    try {
      $request->validate(
        [
          'text' => 'required'
        ]
      );
      $groups = Groub::where('name', 'like', '%' . $request->text . '%')->where('accessibility', 'public')->get(['id', 'name']);
      return response()->json([
        'state' => true,
        'data' => $groups
      ]);
    } catch (\Throwable $th) {
      //throw $th;
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ], 500);
    }
  }

  public function myGroubs()
  {
    try {
      $id = Auth::id();
      $groups = Groub::where('createdBy', $id)->get(['id', 'name']);
      return response()->json([
        'state' => true,
        'data' => $groups
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function addGroup(Request $request)
  {
    try {
      $request->validate([
        'name' => 'required',
        'accessibility' => 'required'
      ]);
      $id = Auth::id();
      $group = Groub::create([
        'createdBy' => $id,
        'name' => $request->name,
        'accessibility' => $request->accessibility,
      ]);
      $userGroup = GroubUser::create([
        'userId' => $id,
        'groubId' => $group['id'],
        'roll' => 'admin'
      ]);
      return response()->json([
        'state' => true,
        'data' => 'added successfuly'
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function allUserGroup()
  {
    try {
      $id = Auth::id();
      $groupIds = GroubUser::where('userId', $id)->pluck('groubId');
      $groups = Groub::whereIn('id', $groupIds)->get([
        'id',
        'name',
        'accessibility',
      ]);
      return response()->json([
        'state' => true,
        'data' => $groups
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function groupInformation(Request $request)
  {
    try {
      $id = Auth::id();
      $request->validate([
        'groupId' => 'required'
      ]);
      $files = File::where('groubId', $request['groupId'])->get(['id', 'name', 'numberOfBookingTimes', 'numberOfTimesModified', 'availability']);
      $groupInformation = array();
      for ($i = 0; $i < count($files); $i++) {
        $groupInformation[$i] = [
          'id' => $files[$i]['id'],
          'name' => $files[$i]['name'],
          'numberOfBookingTimes' => $files[$i]['numberOfBookingTimes'],
          'numberOfTimesModified' => $files[$i]['numberOfTimesModified'],
          'availability' => $files[$i]['availability'],
          'forUser' => $files[$i]['availability'] ? true : Cheak::where('fileId', $files[$i]['id'])->orderBy('created_at','desc')->first()['userId'] == $id
        ];
      }
      return response()->json([
        'state' => true,
        'allFiles' => $groupInformation
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function groupMembers(Request $request)
  {
    try {
      $request->validate([
        'groupId' => 'required'
      ]);
      $usersId = GroubUser::where('groubId', $request['groupId'])->pluck('userId');
      $users = User::whereIn('id', $usersId)->get(['firstName', 'lastName']);
      $res = array();
      for ($i = 0; $i < count($usersId); $i++) {
        $res[$i] = [
          'firstName' => $users[$i]['firstName'],
          'lastName' => $users[$i]['lastName'],
          'roll' => GroubUser::where('groubId', $request['groupId'])->where('userId', $usersId[$i])->value('roll')
        ];
      }
      return response()->json([
        'state' => true,
        'data' => $res
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function addMemberToGroup(Request $request)
  {
    try {
      $id = Auth::id();
      $request->validate([
        'groupId' => 'required',
        'userName' => 'required'
      ]);
      $roll = GroubUser::where('userId', $id)->where('groubId', $request->groupId)->value('roll');
      if ($roll == null || $roll != 'admin') {
        return response()->json([
          'state' => false,
          'data' => 'access denid'
        ], 204);
      }
      $user = User::where('userName', $request->userName)->first();
      if ($user == null) {
        return response()->json([
          'state' => false,
          'data' => 'userName not found'
        ]);
      }
      GroubUser::create([
        'userId' => $user['id'],
        'groubId' => $request->groupId,
        'roll' => 'member'
      ]);
      return response()->json([
        'state' => true,
        'data' => 'added successfuly'
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }
  public function joinGroup(Request $request)  {
    try {
      $id=Auth::id();
      $request->validate([
        'groupId'=>'required'
      ]);
      $userGroup=GroubUser::create([
        "groubId"=>$request->groupId,
        "userId"=>$id,
        "roll"=>"member"
      ]);
      return response()->json([
        'state'=>true
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }
}
