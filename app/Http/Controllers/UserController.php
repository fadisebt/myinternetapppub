<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
  public function login(Request $request)
  {
    try {
      $request->validate([
        'userName' => 'required',
        'password' => 'required'
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ],500);
    }
    $user = User::where('userName', $request->userName)->first();
    if ($user == null) {
      return response()->json([
        'state' => false,
        'data' => 'userName not found'
      ],404);
    }
    if (!Hash::check($request->password, $user['password'])) {
      return response()->json([
        'state' => false,
        'data' => 'wrong password'
      ],304);
    }
    Auth::login($user);
    $token = $user->createToken("api")->plainTextToken;
    return response()->json([
      "state" => true,
      "data" => $token,
      'id'=>$user['id']
    ],200);
  }

  public function register(Request $request)
  {
    try {
      $request->validate([
        'email' => 'required',
        'firstName' => 'required',
        'lastName' => 'required',
        'userName' => 'required',
        'password' => 'required',
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
    try {
      $user = User::create([
        'Email' => $request->email,
        'firstName' => $request->firstName,
        'lastName' => $request->lastName,
        'userName' => $request->userName,
        'password' => Hash::make($request->password)
      ]);
      Auth::login($user);
      $token = $user->createToken("api")->plainTextToken;
      return response()->json([
        "state" => true,
        "data" => $token,
        'id'=>$user['id']
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function logout()
  {
    try {
      $id = Auth::id();
      DB::table('personal_access_tokens')->where('tokenable_id', $id)->delete();
      return response()->json([
        'state' => true,
        'data' => 'logout successful'
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }

  public function profile()
  {
    try {
      $id = Auth::id();
      $user = User::where('id', $id)->first([
        'firstName',
        'lastName',
        'Email',
        'userName'
      ]);
      return response()->json([
        'state' => true,
        'data' => $user
      ]);
    } catch (Exception $th) {
      return response()->json([
        'state' => false,
        'data' => $th->getMessage()
      ]);
    }
  }
}
