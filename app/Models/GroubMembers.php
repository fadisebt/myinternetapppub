<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroubMembers extends Model 
{

    protected $table = 'groubUser';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable=['userId','groubId'];
    protected $hidden=['deleted_at','created_at','updated_at','userId','groubId','roll'];
    protected $appends=['member'];

    public function getMemberAttribute() {
        return UserMemberRoll::where('id',$this->userId)->first();
    }

}
