<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupInformation extends Model
{



    protected $table = 'file';
    public $timestamps = true;

    use SoftDeletes;
    protected $fillable = ['id', 'name', 'numberOfBookingTimes', 'numberOfTimesModified'];
    protected $dates = ['deleted_at'];
    protected $hidden = array(
        'path', 'addBy', 'groubId', 'created_at',
        'updated_at',
        'deleted_at'
    );

    protected $appends = ['availability'];

    public function getAvailabilityAttribute()
    {
        return Cheak::where('fileId', $this->id)->value('availability');
    }
}
