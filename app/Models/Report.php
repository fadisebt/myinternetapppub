<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    protected $table = 'check';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable=['userId','groubId'];
    protected $hidden=['deleted_at','updated_at','userId','fileId','type'];
    protected $appends=['created_at'];

    public function getReportAttribute() {
        return User::where('id',$this->userId)->first(['firstName','lastName']);
    }
}
