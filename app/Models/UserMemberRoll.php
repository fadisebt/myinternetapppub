<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class UserMemberRoll extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'user';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $hidden = array( 'Email', 'password', 'userName', 'place-Of-Residence', 'phonNumber', 'date','id','created_at','updated_at','deleted_at');
    protected $fillable=['firstName','lastName'];
    protected $appends=['roll'];
    public function getRollAttribute() {
        return GroubUser::where('userId',$this->id)->value('roll');
    }
}
