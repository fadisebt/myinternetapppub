<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheakTable extends Migration {

	public function up()
	{
		Schema::create('cheak', function(Blueprint $table) {
			$table->timestamps();
			$table->softDeletes();
			$table->integer('fileId')->unsigned();
			$table->integer('userId')->unsigned();
			$table->string('type')->nullable();
			
		});
	}

	public function down()
	{
		Schema::drop('cheak');
	}
}
