<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileTable extends Migration {

	public function up()
	{
		Schema::create('file', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name')->nullable();
			$table->string('path');
			$table->boolean('availability');
			$table->integer('addBy')->unsigned();
			$table->integer('groubId')->unsigned();
            $table->integer('numberOfBookingTimes')->default(0);
            $table->integer('numberOfTimesModified')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('file');
	}
}
