<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroubUserTable extends Migration {

	public function up()
	{
		Schema::create('groubUser', function(Blueprint $table) {
			$table->timestamps();
			$table->softDeletes();
			$table->integer('userId')->unsigned();
			$table->integer('groubId')->unsigned();
			$table->enum('roll', array('admin', 'member'));
		});
	}

	public function down()
	{
		Schema::drop('groubUser');
	}
}