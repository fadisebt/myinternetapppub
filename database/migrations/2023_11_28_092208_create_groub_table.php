<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroubTable extends Migration {

	public function up()
	{
		Schema::create('groub', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('createdBy')->unsigned();
			$table->string('name');
			$table->enum('accessibility', array('public', 'private'));
		});
	}

	public function down()
	{
		Schema::drop('groub');
	}
}