<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration {

	public function up()
	{
		Schema::create('user', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('firstName');
			$table->string('lastName');
			$table->string('Email')->unique();
			$table->string('password');
			$table->string('userName')->unique();
		});
	}

	public function down()
	{
		Schema::drop('user');
	}
}