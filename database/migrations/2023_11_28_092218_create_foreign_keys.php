<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('file', function(Blueprint $table) {
			$table->foreign('addBy')->references('id')->on('user')
						->onDelete('restrict')
						->onUpdate('cascade');
		});
		Schema::table('groub', function(Blueprint $table) {
			$table->foreign('createdBy')->references('id')->on('user')
						->onDelete('restrict')
						->onUpdate('cascade');
		});
		Schema::table('file', function(Blueprint $table) {
			$table->foreign('groubId')->references('id')->on('groub')
						->onDelete('restrict')
						->onUpdate('cascade');
		});
		Schema::table('groubUser', function(Blueprint $table) {
			$table->foreign('userId')->references('id')->on('user')
						->onDelete('restrict')
						->onUpdate('cascade');
		});
		Schema::table('groubUser', function(Blueprint $table) {
			$table->foreign('groubId')->references('id')->on('groub')
						->onDelete('restrict')
						->onUpdate('cascade');
		});
		Schema::table('cheak', function(Blueprint $table) {
			$table->foreign('fileId')->references('id')->on('file')
						->onDelete('restrict')
						->onUpdate('cascade');
		});
		Schema::table('cheak', function(Blueprint $table) {
			$table->foreign('userId')->references('id')->on('user')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('file', function(Blueprint $table) {
			$table->dropForeign('file_addBy_foreign');
		});
		Schema::table('file', function(Blueprint $table) {
			$table->dropForeign('file_groubId_foreign');
		});
		Schema::table('groubUser', function(Blueprint $table) {
			$table->dropForeign('groubUser_userId_foreign');
		});
		Schema::table('groubUser', function(Blueprint $table) {
			$table->dropForeign('groubUser_groubId_foreign');
		});
		Schema::table('cheak', function(Blueprint $table) {
			$table->dropForeign('cheak_fileId_foreign');
		});
	}
}
