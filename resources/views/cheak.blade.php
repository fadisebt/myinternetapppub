{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('userId', 'UserId:') !!}
			{!! Form::text('userId') !!}
		</li>
		<li>
			{!! Form::label('fileId', 'FileId:') !!}
			{!! Form::text('fileId') !!}
		</li>
		<li>
			{!! Form::label('availability', 'Availability:') !!}
			{!! Form::text('availability') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}