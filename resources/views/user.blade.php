{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('firstName', 'FirstName:') !!}
			{!! Form::text('firstName') !!}
		</li>
		<li>
			{!! Form::label('lastName', 'LastName:') !!}
			{!! Form::text('lastName') !!}
		</li>
		<li>
			{!! Form::label('Email', 'Email:') !!}
			{!! Form::text('Email') !!}
		</li>
		<li>
			{!! Form::label('password', 'Password:') !!}
			{!! Form::text('password') !!}
		</li>
		<li>
			{!! Form::label('userName', 'UserName:') !!}
			{!! Form::text('userName') !!}
		</li>
		<li>
			{!! Form::label('place-Of-Residence', 'Place-Of-Residence:') !!}
			{!! Form::text('place-Of-Residence') !!}
		</li>
		<li>
			{!! Form::label('phonNumber', 'PhonNumber:') !!}
			{!! Form::text('phonNumber') !!}
		</li>
		<li>
			{!! Form::label('date', 'Date:') !!}
			{!! Form::text('date') !!}
		</li>
		<li>
			{!! Form::label('roll', 'Roll:') !!}
			{!! Form::text('roll') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}