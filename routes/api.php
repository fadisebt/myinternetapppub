<?php

use App\Http\Controllers\CheakController;
use App\Http\Controllers\GroubController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\FileController;
use App\Models\Cheak;
use App\Models\Groub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login',[UserController::class,'login']);
Route::post('/register',[UserController::class,'register']);
Route::get('/logout',[UserController::class,'logout'])->middleware('auth:sanctum');
Route::get('/profile',[UserController::class,'profile'])->middleware('auth:sanctum');
Route::post('/addGroup',[GroubController::class,'addGroup'])->middleware('auth:sanctum');
Route::get('/allUserGroup',[GroubController::class,'allUserGroup'])->middleware('auth:sanctum');
Route::post('/addMemberToGroup',[GroubController::class,'addMemberToGroup'])->middleware('auth:sanctum');
Route::post('/groupInformation',[GroubController::class,'groupInformation'])->middleware('auth:sanctum');
Route::post('/groupMembers',[GroubController::class,'groupMembers']);
Route::post('/search',[GroubController::class,'search']);
Route::get('/myGroubs',[GroubController::class,'myGroubs'])->middleware('auth:sanctum');
Route::post('/joinGroup',[GroubController::class,'joinGroup'])->middleware('auth:sanctum');

/**
 * file
 */
Route::post('/uplodFile',[FileController::class,'uplodFile'])->middleware('auth:sanctum');
Route::get('/showFile/{userId}/{id}',[FileController::class,'showFile']);
Route::get('/report/{id}',[FileController::class,'report']);
Route::post('/update',[FileController::class,'updat'])->middleware('auth:sanctum');
/**
 * check
 */
Route::post('/checkOut',[CheakController::class,'checkOut']);
Route::post('/checkIn',[CheakController::class,'checkIn'])->middleware('auth:sanctum');
